package com.example.puzzlen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.IOException;

public class imagenesJuego extends AppCompatActivity implements View.OnClickListener{
    private Intent i;
    private MediaPlayer mp;
    private static boolean reproduciendo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagenes_juego);

        Button btnPlay= (Button)findViewById(R.id.sonidoOn);
        Button btnStop= (Button)findViewById(R.id.sonidoOff);

        btnPlay.setOnClickListener(this);
        btnStop.setOnClickListener(this);

    }

    public void onClick(View v) {
        //Comprobamos el identificador del boton que ha llamado al evento para ver
        //cual de los botones es y ejecutar la acción correspondiente
        switch(v.getId()){
            case R.id.sonidoOn:
                if(!reproduciendo) {//verifica que ya no se este reproduciendo
                    mp = MediaPlayer.create(this, R.raw.musica);
                    mp.setLooping(true);
                    mp.start();
                    reproduciendo=true;
                }
                break;

            case R.id.sonidoOff:
                //Paramos el audio y volvemos a inicializar
                if(reproduciendo) {
                    mp.stop();
                    mp.setLooping(false);
                    reproduciendo=false;
                }
                break;
        }
    }
}

