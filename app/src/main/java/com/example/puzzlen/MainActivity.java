package com.example.puzzlen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import androidx.core.content.ContextCompat;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private boolean favorito = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Puzzle);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    private void setFavoriteIcon(MenuItem item) {

        int id = this.favorito ? R.drawable.ic_baseline_favorite_24 : R.drawable.ic_baseline_favorite_border_24;
        item.setIcon(ContextCompat.getDrawable((Context)this, id));
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.favorito:
                this.favorito = !this.favorito;
                this.setFavoriteIcon(item);
                break;

            case R.id.ayuda:
                Toast.makeText(this.getApplicationContext(), (CharSequence)"Ayuda",Toast.LENGTH_LONG).show();
                WebView myWebView = new WebView(this.getApplicationContext());
                this.setContentView((View)myWebView);
                myWebView.loadUrl("file:///android_asset/juego.html");
                break;

            case R.id.salir:
                System.exit(0);
                break;
            case R.id.compartir:
                Intent send = new Intent();
                send.putExtra("android.intent.extra.TEXT", "https://gitlab.com/MonchoUOC/puzzle.git");
                send.setType("text/plain");
                send = Intent.createChooser(send, (CharSequence)null);
                this.startActivity(send);
                break;


            default:
                super.onOptionsItemSelected(item);
        }
        return false;
    }




    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.barra, menu);
        return true;
    }



    public void exit(View v){
        System.exit(0);

    }
    public void imagenesJuego(View v) {
        Intent intent = new Intent((Context)this, imagenesJuego.class);
        this.startActivity(intent);
    }

    public void  onBackPressed() {
        super.onBackPressed();
        //Toast.makeText(applicationContext, "Ayuda", Toast.LENGTH_LONG).show()
        Intent  intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

}

